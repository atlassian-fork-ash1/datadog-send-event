# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.1

- patch: Update the Readme with a new Atlassian Community link.

## 1.1.0

- minor: Add argument to choose endpoint

## 1.0.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 1.0.0

- major: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.4

- patch: Updated contributing guidelines

## 0.2.3

- patch: Fixed the issue with doublequotes in a payload

## 0.2.2

- patch: Add default text to readme

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.1

- patch: Improve task logging

## 0.1.0

- minor: Initial version

